Construction et maintenance de logiciels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce dépôt contient des fragments de code présentés dans le cadre du cours
INF3135 Construction et maintenance de logiciels à l'été 2017.

*Avertissement:* Il peut y avoir des bogues et le code n'est pas toujours bien
documenté.

Contenu
=======

Les fichiers sont répartis selon les chapitres :

- ``chapitre02``: Introduction au langage C
- ``chapitre03``: Introduction au langage C (suite)
- ``chapitre04``: Entrées et sorties
- ``chapitre05``: Structures de données

License
=======

Tout le contenu de ce projet est sous `license GPLv3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`__.
